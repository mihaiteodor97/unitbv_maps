package com.unitbv.unitbvonline.jsonparser

import android.content.Context
import android.graphics.*
import com.unitbv.unitbvonline.R
import org.json.JSONArray
import org.json.JSONObject
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.Projection
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.PathOverlay
import org.osmdroid.views.overlay.Polyline
import java.io.InputStream

class Parser {

    fun parseWaypoints(map : MapView, inpStream: InputStream, ctx: Context)
    {
        var line = inpStream.bufferedReader().use{it.readText()}
        var jsonObj = JSONObject(line)
        var universityCampus : JSONArray = jsonObj.getJSONArray("buildings")

        for(i in 0 until (universityCampus.length()) )
        {
            var building = universityCampus.getJSONObject(i)

            val marker = Marker(map)
            marker.icon = ctx.resources.getDrawable(R.mipmap.unitbv_launcher_foreground)
            marker.subDescription = building.getString("subDescription")
            marker.title = building.getString("title")
            marker.position = GeoPoint(building.getDouble("lat"),
                building.getDouble("lon"))
            marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
            map.overlays.add(marker)

        }
    }

    fun parseRoute(data: String, map: MapView)
    {
        if(data == "")
            return

        var waypointsArray : JSONArray = JSONObject(data).getJSONArray("waypoints")

        var line = Polyline()
        line.setWidth(20f)
        var pts = mutableListOf<GeoPoint>()

        for(i in 0 until waypointsArray.length())
        {
            var waypoint = waypointsArray.getJSONObject(i)
            var locationCoords: JSONArray = waypoint.getJSONArray("location")
            var lat = locationCoords.getDouble(0)
            var lon = locationCoords.getDouble(1)

            print(lat)
            print(" ")
            println(lon)

            pts.add(GeoPoint(lat, lon))
        }

        line.setPoints(pts)
        line.setGeodesic(true)
        map.overlayManager.add(line)
        map.invalidate()
    }
}