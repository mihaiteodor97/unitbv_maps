package com.unitbv.unitbvonline.ui

import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.unitbv.unitbvonline.R

class WebPageActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.web_view_layout)
        // Find web view
        var webView : WebView =  findViewById(R.id.web_view_layout)

        // set web view client
        webView.setWebViewClient(WebViewClient())

        //clear cache
        webView.clearCache(true)

        webView.getSettings().setJavaScriptEnabled(true)

        //load url
        webView.loadUrl("https://www.unitbv.ro/")

    }
}