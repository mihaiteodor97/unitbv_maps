package com.unitbv.unitbvonline.osrmrequester
import com.unitbv.unitbvonline.jsonparser.Parser
import org.osmdroid.views.MapView
import java.io.DataOutputStream
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.nio.charset.StandardCharsets
import kotlin.concurrent.thread

class RequestManager {

    var returnValue: String

    init{
        returnValue = ""
    }

    fun sendRequest(map:MapView)
    {
        thread{
            val url = "http://router.project-osrm.org/route/v1/driving/45.7104,25.5652;45.638895,25.583726?overview=false"
            val reqURL = URL(url)

            with(reqURL.openConnection() as HttpURLConnection) {
                requestMethod = "GET"  // optional default is GET

                println("\nSent 'GET' request to URL : $url; Response Code : $responseCode")
                if (responseCode == 200) {
                    inputStream.bufferedReader().use {
                        it.lines().forEach { line ->
                            returnValue = returnValue + line
                        }
                    }
                }

                val parser = Parser()
                parser.parseRoute(returnValue, map)
            }
       }


    }

    fun getRequestResult(): String{
        return returnValue
    }
}