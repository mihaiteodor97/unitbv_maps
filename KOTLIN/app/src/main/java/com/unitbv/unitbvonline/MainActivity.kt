package com.unitbv.unitbvonline

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.preference.PreferenceManager
import android.os.Build
import android.os.Looper
import android.provider.Settings
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.unitbv.unitbvonline.jsonparser.Parser
import com.unitbv.unitbvonline.osrmrequester.RequestManager
import kotlinx.android.synthetic.main.osm_main.*
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import java.io.InputStream


class MainActivity : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener,
    LocationListener {
    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onConnectionSuspended(p0: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    var currentPos: GeoPoint

    private var mFusedLocationProviderClient: FusedLocationProviderClient? = null
    private val INTERVAL: Long = 2000
    private val FASTEST_INTERVAL: Long = 1000
    internal lateinit var mLocationRequest: LocationRequest
    private val REQUEST_PERMISSION_LOCATION = 10

    private lateinit var mGoogleApiClient: GoogleApiClient

    init{
        currentPos = GeoPoint(0.0,0.0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        checkPermissionForLocation(this.applicationContext)

        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()

        mLocationRequest = LocationRequest()

        var lm: LocationManager = getSystemService(LOCATION_SERVICE) as LocationManager
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER))
            buildAlertMessageNoGps()

        val ctx = this.applicationContext

        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx))
        Configuration.getInstance().userAgentValue = BuildConfig.APPLICATION_ID
        setContentView(R.layout.osm_main)

        var map: MapView = findViewById(R.id.mapview)

        //SETTING TILE SOURCE
        val tileSource = TileSourceFactory.OpenTopo
        map.setTileSource(tileSource)
        map.zoomController.setVisibility(CustomZoomButtonsController.Visibility.NEVER)
        map.setMultiTouchControls(true)

        //ADDING UNIVERSITY BUILDINGS WAYPOINTS
        val inpStream:InputStream = assets.open("waypoints.json")
        var parser = Parser()
        parser.parseWaypoints(map, inpStream, ctx)

        //CENTERING MAP AND SETTING ZOOM LEVELS
        map.controller.setZoom(13.0)
        map.minZoomLevel = 13.0

        centerMapEvent(map)
    }

    override fun onConnected(p0: Bundle?) {
        startLocationUpdates()
    }

    override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect()
        }
    }

    fun toggleMenu(v : View)
    {
        val intent = Intent(this, Menu::class.java)
        startActivity(intent)
    }

    fun calculateRoute(v: View)
    {
        centerMapEvent(this.mapview)

        if(currentPosInBrasov())
        {
            val rm = RequestManager()
            rm.sendRequest(this.mapview)
        }
        else
        {
            AlertDialog.Builder(this@MainActivity)
                .setTitle("Nu esti in Brasov!")
                .setMessage("Serviciile de routing UNITBV Online sunt restrictionate la orasul Brasov!")
                .show()
        }
    }


    fun centerMapEvent(v: View)
    {
        this.mapview.controller.setCenter(currentPos)

       // print(currentPos.latitude)
      //  print(" ")
     //   println(currentPos.longitude)

        var existing = this.mapview.overlays.filterIsInstance<Marker>().toList()
        if(existing.any { it.title == "Pozitia ta!" })
        {
            existing.find{it.title == "Pozitia ta!"}?.position = currentPos
            existing.find{it.title == "Pozitia ta!"}?.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
        }
        else{
            var myLocationMarker = Marker(this.mapview)
            myLocationMarker.title = "Pozitia ta!"
            myLocationMarker.position = currentPos
            myLocationMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
            this.mapview.overlays.add(myLocationMarker)
        }
    }

    fun currentPosInBrasov(): Boolean
    {
        if(currentPos.latitude < 45.701455 && currentPos.latitude > 45.611426
            && currentPos.longitude < 25.675851 && currentPos.longitude > 25.551004)
            return true
        return false
    }

        private fun buildAlertMessageNoGps() {

            val builder = AlertDialog.Builder(this)
            builder.setMessage("Unitbv Online are nevoie de activarea serviciilor GPS")
                .setCancelable(false)
                .setPositiveButton("Ok") { dialog, id ->
                    startActivityForResult(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        , 11)
                }
                .setNegativeButton("Cancel") { dialog, id ->
                    dialog.cancel()
                    finish()
                }
            val alert: AlertDialog = builder.create()
            alert.show()

        }

        protected fun startLocationUpdates() {

            // Create the location request to start receiving updates

            mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            mLocationRequest!!.setInterval(INTERVAL)
            mLocationRequest!!.setFastestInterval(FASTEST_INTERVAL)

            // Create LocationSettingsRequest object using location request
            val builder = LocationSettingsRequest.Builder()
            builder.addLocationRequest(mLocationRequest!!)
            val locationSettingsRequest = builder.build()

            val settingsClient = LocationServices.getSettingsClient(this)
            settingsClient.checkLocationSettings(locationSettingsRequest)

            mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
            // new Google API SDK v11 uses getFusedLocationProviderClient(this)
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return
            }

            mFusedLocationProviderClient!!.requestLocationUpdates(mLocationRequest, mLocationCallback,
                Looper.myLooper())

        }

        private var mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                // do work here
                locationResult.lastLocation
                onLocationChanged(locationResult.lastLocation)
            }
        }

        override fun onLocationChanged(location: Location) {
            // New location has now been determined

            currentPos = GeoPoint(location.latitude, location.longitude)

            centerMapEvent(this.mapview)

            // You can now create a LatLng Object for use with maps
        }

        fun stoplocationUpdates() {
            mFusedLocationProviderClient!!.removeLocationUpdates(mLocationCallback)
        }


        override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
            if (requestCode == REQUEST_PERMISSION_LOCATION) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates()
                } else {
                    Toast.makeText(this@MainActivity, "Permission Denied", Toast.LENGTH_SHORT).show()
                }
            }
        }

        fun checkPermissionForLocation(context: Context): Boolean {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED) {
                    true
                } else {
                    // Show the permission request
                    ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        REQUEST_PERMISSION_LOCATION)
                    false
                }
            } else {
                true
            }
        }
}
